package com.exhibition

import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Query

interface BlinkApi {

    @GET("exhibitions")
    fun getExhibitions(@Query("page") page: Int,
                         @Query("size") size: Int): Deferred<JsonObject>

    @GET("tickets")
    fun getTickets(@Query("page") page: Int,
                       @Query("size") size: Int): Deferred<JsonObject>

    @GET("exhibits")
    fun getExhibits(@Query("page") page: Int,
                    @Query("size") size: Int, @Query("exhibitionId") exhibitionId: String): Deferred<JsonObject>

    @PUT("tickets/add")
    fun addTicket(@Query("priceId") priceId: String): Call<JsonObject>
}