package com.exhibition

import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.http.*

interface BlinkLoginApi {
    @POST("oauth/token")
    @FormUrlEncoded
    fun loginUser(@Field("username") username: String, @Field("password") password: String,
                  @Field("grant_type") grantType: String): Deferred<JsonObject>

    @POST("users/register")
    @FormUrlEncoded
    fun registerUser(@Field("email") email: String,
                     @Field("password") password: String): Deferred<JsonObject>

    @POST("password/reset")
    @FormUrlEncoded
    fun resetPassword(@Field("email") email: String): Deferred<JsonObject>

    @PUT("users/resendVerification")
    fun resendVerification(@Query("email") email: String): Deferred<JsonObject>

    @POST("oauth/token")
    @FormUrlEncoded
    fun refreshToken(@Field("grant_type") grantType: String,
                     @Field("refresh_token") refresh_token: String): Deferred<JsonObject>

    @POST("oauth/token")
    @FormUrlEncoded
    fun refreshTokenFromWorker(@Field("grant_type") grantType: String,
                               @Field("refresh_token") refresh_token: String): Call<JsonObject>
}