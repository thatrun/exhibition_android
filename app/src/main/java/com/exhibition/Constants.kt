package com.exhibition

import java.text.SimpleDateFormat
import java.util.*


class Constants {
    companion object {
        const val SHARED_PREFERENCES = "SHARED_PREFERENCES"
        const val BLINK_ENDPOINT = "http://46.101.223.148:8080/"
        const val TOKEN = "TOKEN"
        const val REFRESH_TOKEN = "REFRESH_TOKEN"
        const val LAST_LOCATION_UPDATE_WAIT_TIME = 5000L
        const val PAGINATION_ITEMS_SIZE = 10
        const val LOCATION_PERMISSION_REQUEST_CODE = 100
        const val POSITION = "POSITION"
        const val SCHEDULE_POSITION = "SCHEDULE_POSITION"
        const val SCHEDULE_DATES_POSITION = "SCHEDULE_DATES_POSITION"
        const val BLINK_DATABASE = "BLINK_DATABASE"
        const val CURRENT_USERNAME = "CURRENT_USERNAME"
        const val BLINK_CLIENT_SECRET = "c3ByaW5nLXNlY3VyaXR5LW9hdXRoMi1yZWFkLXdyaXRlLWNsaWVudDpzcHJpbmctc2VjdXJpdHktb2F1dGgyLXJlYWQtd3JpdGUtY2xpZW50LXBhc3N3b3JkMTIzNA=="
        const val CURRENT_USER_PHOTO = "CURRENT_USER_PHOTO"
        const val CURRENT_USER_ID = "CURRENT_USER_ID"
        const val INTEREST_ACTION = "INTEREST_ACTION"
        const val ADD_INTEREST = 1
        const val REMOVE_INTEREST = 2
        const val ERROR_MESSAGE = "ERROR_MESSAGE"
        const val INTERESTS_WORKER = "INTERESTS_WORKER"
        const val NOTIFICATIONS_WORKER = "NOTIFICATIONS_WORKER"
        const val TIME = "TIME"
        const val TITLE = "TITLE"
        const val EVENT_ID = "EVENT_ID"
        const val EVENT_CATEGORIES = "EVENT_CATEGORIES"
        const val EVENT_TITLE = "EVENT_TITLE"
        const val EVENT_DESCRIPTION = "EVENT_DESCRIPTION"
        const val EVENT_IMAGE = "EVENT_IMAGE"
        const val ORGANIZER_FIRST_NAME = "ORGANIZER_FIRST_NAME"
        const val ORGANIZER_LAST_NAME = "ORGANIZER_FIRST_NAME"
        const val ORGANIZER_USERNAME = "ORGANIZER_USERNAME"
        const val ORGANIZER_IMAGE = "ORGANIZER_IMAGE"
        const val ORGANIZER_ID = "ORGANIZER_ID"
        const val IS_USER_INTERESTED = "IS_USER_INTERESTED"
        const val FOLLOW_ID = "FOLLOW_ID"
        const val FOLLOW_ACTION = "FOLLOW_ACTION"
        const val ADD_FOLLOW = 0
        const val REMOVE_FOLLOW = 1
        const val FOLLOW_WORKER = "FOLLOW_WORKER"
        const val LAT_LNG_BOUNDS = "LAT_LNG_BOUNDS"
        const val DESCRIPTION_MAX_LINES = 3
        const val POPULAR_CITY_SEARCH_TYPE = 0
        const val POPULAR_EVENT_TYPE = 1
        const val LAST_CITY_SEARCH = "LAST_CITY_SEARCH"
        const val STORAGE_PERMISSION_CODE = 12
        const val IMAGE_CODE = 22
        const val INTEREST_BROADCAST = "ua.blink.InterestBroadcast"
        const val FOLLOW_BROADCAST = "ua.blink.FollowBroadcast"
        const val TOKEN_REFRESHED_FROM_WORKER_BROADCAST = "ua.blink.TokenRefreshedFromWorkerBroadcast"
        const val EVENT_ITEM_TYPE = 1
        const val DATE_ITEM_TYPE = 2
        const val LOCATIONS_FROM_EXPLORE = 1
        const val LOCATIONS_FROM_PROFILE_EDIT = 2
        const val SCROLL_POSITION = "SCROLL_POSITION"
        const val CURRENT_USER = "CURRENT_USER"
        const val FOLLOWING_INFO_TYPE = 0
        const val FOLLOWERS_INFO_TYPE = 1
        const val TYPE = "TYPE"
        const val DATA_LOADED = "IS_DATA_LOADED"
        const val CONTENT = "CONTENT"
        const val PAGE = "PAGE"
        const val COMPLAIN_WORKER = "COMPLAIN_WORKER"
        const val COMPLAIN_CAUSE = "COMPLAIN_CAUSE"
        const val POPULAR_PAGE = "POPULAR_PAGE"
        const val POPULAR_CONTENT = "POPULAR_CONTENT"
        const val POPULAR_SCROLL_POSITION = "POPULAR_SCROLL_POSITION"
        const val FOLLOWING_PAGE = "FOLLOWING_PAGE"
        const val FOLLOWING_CONTENT = "FOLLOWING_CONTENT"
        const val FOLLOWING_SCROLL_POSITION = "FOLLOWING_SCROLL_POSITION"
        const val INTERESTS_PAGE = "INTERESTS_PAGE"
        const val INTERESTS_CONTENT = "INTERESTS_CONTENT"
        const val INTERESTS_SCROLL_POSITION = "INTERESTS_SCROLL_POSITION"
        const val PROFILE_SCROLL_POSITION = "PROFILE_SCROLL_POSITION"
        const val PROFILE_LATEST_CREATED_EVENT_POSITION = "PROFILE_LATEST_CREATED_EVENT_POSITION"
        const val CITIES_CONTENT = "CITIES_CONTENT"
        const val CITIES_PAGE = "CITIES_PAGE"
        const val CACHING_ITEMS_MAX_COUNT = 10
        const val MAP_PADDING_ACTION_INCREASE = 1
        const val MAP_PADDING_ACTION_DECREASE = 2
        const val ERROR_HANDLING_REVEAL_TIME = 2700L
        const val POSITION_SEARCHING_BAR_TYPE = 9
        const val POSITION_CURRENT_USER_LOCATION_TYPE = 10
        const val POSITION_CURRENT_SEARCH_TYPE = 11
        const val POSITION_TYPE = 12
        const val BLINK_EVENTS_CHANNEL_NOTIFICATION_ID = "3971a534-c149-4347-a386-fa67b8b61186"
        const val BLINK_EVENTS_NOTIFICATION_TAG = "BLINK_EVENTS_NOTIFICATION_TAG"
        const val BLINK_INTERESTS_NOTIFICATION_TAG = "BLINK_INTERESTS_NOTIFICATION_TAG"
        const val BLINK_FOLLOWS_NOTIFICATION_TAG = "BLINK_FOLLOWS_NOTIFICATION_TAG"
        const val BLINK_EVENTS_STARTING_NOTIFICATION_TAG = "BLINK_EVENTS_STARTING_NOTIFICATION_TAG"
        const val MAP_ZOOM_MIN_VALUE = 10f
        const val MAP_ZOOM = "MAP_ZOOM"
        const val MAP_LAT_LNG = "MAP_LAT_LNG"
        const val LAST_LOCATION_APPLIED = "LAST_LOCATION_APPLIED"
        const val WORKER_ERROR_TYPE = "WORKER_ERROR_TYPE"
        const val NOTIFICATION_ERROR_TYPE = 1
        const val INTEREST_ERROR_TYPE = 2
        const val VISIBLE_EVENTS = "VISIBLE_EVENTS"
        const val IS_RECYCLER_MOVED = "IS_RECYCLER_MOVED"
        const val LOCATION_REQUEST_UNSUCCESSFUL = "LOCATION_REQUEST_UNSUCCESSFUL"
        const val CURRENT_LOCATION = "CURRENT_LOCATION"
        const val SCHEDULE_DATES_PAGE = "SCHEDULE_DATES_PAGE"
        const val SCHEDULE_PAGE = "SCHEDULE_PAGE"
        const val SCHEDULE_DATES_CONTENT = "SCHEDULE_DATES_CONTENT"
        const val SCHEDULE_CONTENT = "SCHEDULE_CONTENT"
        const val SCHEDULE_CHOSEN_DATE = "SCHEDULE_CHOSEN_DATE"
        const val DETAILED_MORE_BTN_CLICKED = "DETAILED_MORE_BTN_CLICKED"
        const val LOADED_EVENTS_BOUNDS = "LOADED_EVENTS_BOUNDS"
        const val LAST_EVENT_BOUNDS_TO_CHECK = "LAST_EVENT_BOUNDS_TO_CHECK"
        const val WAS_NOTIFICATION_DIALOG_SHOWN = "WAS_NOTIFICATION_DIALOG_SHOWN"
        const val ERRORS_COUNT = "ERRORS_COUNT"
        const val ERRORS_DISABLED_COUNT = "ERRORS_DISABLED_COUNT"

        const val NOTIFICATION_TYPE_EVENT_CREATED = 1
        const val NOTIFICATION_TYPE_USER_ATTENDED = 2
        const val NOTIFICATION_TYPE_USER_FOLLOWED = 3
        const val NOTIFICATION_TYPE_EVENT_STARTING = 4

        const val CREATED_UPCOMING_EVENTS = 0
        const val CREATED_PAST_EVENTS = 1

        const val CITY_CHOSEN = 1
        const val CITY_ALREADY_CHOSEN = 2

        const val DETAILS_TOP_BAR_TRANSPARENT = 0
        const val DETAILS_TOP_BAR_NOT_TRANSPARENT = 1
        const val DETAILS_TOP_BAR_STATE = "DETAILS_TOP_BAR_STATE"


        var monthDateFormat = SimpleDateFormat("MMM", Locale.getDefault())
        var weekDateFormat = SimpleDateFormat("EE", Locale.getDefault())
        var dayDateFormat = SimpleDateFormat("d", Locale.getDefault())
        var timeDateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        var dateFormat = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())

    }

}