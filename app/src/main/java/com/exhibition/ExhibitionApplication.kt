package com.exhibition

import android.app.Application
import androidx.work.Configuration
import ua.blink.di.AppComponent
import ua.blink.di.AppModule
import ua.blink.di.DaggerAppComponent

class ExhibitionApplication: Application(), Configuration.Provider {

    companion object {
        @JvmStatic lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
        appComponent.inject(this)
    }

    override fun getWorkManagerConfiguration(): Configuration = Configuration.Builder()//https://developer.android.com/topic/libraries/architecture/workmanager/advanced/custom-configuration#remove-default
        .setMinimumLoggingLevel(android.util.Log.INFO)
        .build()

}