package com.exhibition

interface ExhibitionClickListener {
    fun onClick(position: Int)
}