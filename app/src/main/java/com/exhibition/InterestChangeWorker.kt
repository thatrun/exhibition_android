package com.exhibition

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class InterestChangeWorker(context: Context, workerParams: WorkerParameters): Worker(context, workerParams) {

    var eventId: String = ""
    var blinkApi: BlinkApi? = null
    var blinkLoginApi: BlinkLoginApi? = null
    var sharedPreferences: SharedPreferences? = null

    override fun doWork(): Result {
        initTools()
        eventId = inputData.getString(Constants.EVENT_ID)?:""
        return try {
            loadInterest()
            Log.d(Constants.INTERESTS_WORKER, "SUCCESS ${eventId}")
            Result.success()
        } catch (t: Throwable) {
            val message = try {
                Gson().fromJson((t as? HttpException?)?.response()?.errorBody()?.
                    string(), JsonObject::class.java).getAsJsonPrimitive("error").asString
            } catch (e: Throwable) {
                "${t.message}"
            }
            val intent = Intent(Constants.INTEREST_BROADCAST)//данный broadcast отвечает за обработку ошибки из изменения уведомления или интереса
            intent.putExtra(Constants.EVENT_ID, eventId)
            intent.putExtra(Constants.ERROR_MESSAGE, message)
            intent.putExtra(Constants.WORKER_ERROR_TYPE, Constants.INTEREST_ERROR_TYPE)//так как мы работаем с интересом
            applicationContext.sendBroadcast(intent)
            Result.failure()
        }
    }

    private fun loadInterest() {
        val request = blinkApi?.addTicket(eventId)?.execute()
        checkApiError(request)
    }

    private fun checkApiError(request: retrofit2.Response<JsonObject>?) {
        if (request?.isSuccessful == false && request.code() == 401) {
            val refreshRequest = blinkLoginApi?.refreshTokenFromWorker("refresh_token",
                sharedPreferences?.getString(Constants.REFRESH_TOKEN, "")?:"")?.execute()
            if (refreshRequest?.isSuccessful == true) {
                sharedPreferences?.edit()?.putString(Constants.TOKEN, refreshRequest?.body()?.getAsJsonPrimitive("access_token")?.asString?:"")?.commit()
                initTools()
                applicationContext.sendBroadcast(Intent(Constants.TOKEN_REFRESHED_FROM_WORKER_BROADCAST))
                loadInterest()
            } else if (refreshRequest?.isSuccessful == false)
                throw HttpException(refreshRequest)
        } else if (request?.isSuccessful == false)
            throw HttpException(request)
    }

    private fun initTools() {
        sharedPreferences = applicationContext.getSharedPreferences(Constants.SHARED_PREFERENCES, 0)
        val interceptor = object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val request: Request = chain.request().newBuilder()
                    .addHeader("Authorization",
                        "Bearer ${sharedPreferences?.getString(Constants.TOKEN, "")}")
                    .build()
                return chain.proceed(request)
            }
        }
        val retrofit = Retrofit.Builder().baseUrl(Constants.BLINK_ENDPOINT)
            .client(
                OkHttpClient.Builder().addInterceptor(interceptor)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS).build())
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder()
                        .setLenient()
                        .create()))
            .build()
        blinkApi = retrofit.create(BlinkApi::class.java)

        val interceptorLogin = object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val request: Request = chain.request().newBuilder()
                    .addHeader("Authorization",
                        "Basic ${Constants.BLINK_CLIENT_SECRET}")
                    .build()
                return chain.proceed(request)
            }
        }
        val retrofitLogin = Retrofit.Builder().baseUrl(Constants.BLINK_ENDPOINT)
            .client(OkHttpClient.Builder().addInterceptor(interceptorLogin).build())
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder()
                        .setLenient()
                        .create()))
            .build()
        blinkLoginApi = retrofitLogin.create(BlinkLoginApi::class.java)
    }
}