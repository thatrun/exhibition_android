package com.exhibition

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.exhibition.databinding.ActivityMainBinding
import ua.blink.di.main.DaggerMainComponent
import ua.blink.di.main.MainComponent
import ua.blink.di.main.MainModule

class MainActivity : AppCompatActivity() {

    companion object {
        @JvmStatic lateinit var mainComponent: MainComponent
    }
    lateinit var binder: ActivityMainBinding
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainComponent = DaggerMainComponent.builder()
            .mainModule(MainModule(this))
            .appComponent(ExhibitionApplication.appComponent)
            .build()
        mainComponent.inject(this)
        binder = DataBindingUtil.setContentView(this, R.layout.activity_main)
        navController = findNavController(R.id.main_navigation_fragment)
        setSupportActionBar(binder.mainToolbar)
        binder.mainBottomNavigation.setupWithNavController(navController)
        val appBarConfiguration = AppBarConfiguration.Builder(R.id.exhibitionFragment, R.id.ticketFragment).build()
        NavigationUI.setupWithNavController(binder.mainToolbar, navController, appBarConfiguration)
    }

    fun hideActionBar() {
        supportActionBar?.hide()
    }

    fun showActionBar() {
        supportActionBar?.show()
    }

    fun showBottomBar() {
        binder.mainBottomNavigation.visibility = View.VISIBLE
    }

    fun hideBottomBar() {
        binder.mainBottomNavigation.visibility = View.GONE
    }

    override fun onBackPressed() {
        if (navController.currentDestination?.id == R.id.loginFragment ||
            navController.currentDestination?.id == R.id.registrationFragment ||
            navController.currentDestination?.id == R.id.exhibitionDetailed)
            super.onBackPressed()
        else
            finish()
    }
}
