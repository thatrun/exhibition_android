package com.exhibition

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.exhibition.models.Exhibition
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(application: Application): AndroidViewModel(application) {

    var exhibitionsLiveData: MutableLiveData<Pair<Int, ArrayList<Exhibition>>> = MutableLiveData()
    var exhibitions: ArrayList<Exhibition> = arrayListOf()
    var exhibitionsPage: Int = 0
    var exhibitionsErrorSingleLiveEvent: SingleLiveEvent<String> = SingleLiveEvent()

    var ticketsLiveData: MutableLiveData<Pair<Int, ArrayList<Exhibition>>> = MutableLiveData()
    var tickets: ArrayList<Exhibition> = arrayListOf()
    var ticketsPage: Int = 0
    var ticketsErrorSingleLiveEvent: SingleLiveEvent<String> = SingleLiveEvent()

    fun onLoadExhibitions(api: BlinkApi) {
        if (exhibitionsPage >= 0) {
            exhibitionsPage += 1
            viewModelScope.launch(Dispatchers.IO) {
                val result = api.getExhibitions(exhibitionsPage, Constants.PAGINATION_ITEMS_SIZE)
                withContext(Dispatchers.Main) {
                    try {
                        val t = result.await()
                        val listType = object: TypeToken<ArrayList<Exhibition>>(){}.type
                        exhibitions.addAll(Gson().fromJson(t.getAsJsonArray("content"), listType))
                        if (exhibitionsPage >= t.getAsJsonPrimitive("totalPages").asInt)
                            exhibitionsPage = -200

                        exhibitionsLiveData.value = Pair(exhibitionsPage, exhibitions)
                    } catch (e: Throwable) {
                        exhibitionsErrorSingleLiveEvent.value = e.message?:""
                    }
                }
            }
        }
    }

    fun onLoadTickets(api: BlinkApi) {
        if (ticketsPage >= 0) {
            ticketsPage += 1
            viewModelScope.launch(Dispatchers.IO) {
                val result = api.getTickets(ticketsPage, Constants.PAGINATION_ITEMS_SIZE)
                withContext(Dispatchers.Main) {
                    try {
                        val t = result.await()
                        val listType = object: TypeToken<ArrayList<Exhibition>>(){}.type
                        tickets.addAll(Gson().fromJson(t.getAsJsonArray("content"), listType))
                        if (ticketsPage >= t.getAsJsonPrimitive("totalPages").asInt)
                            ticketsPage = -200

                        ticketsLiveData.value = Pair(ticketsPage, tickets)
                    } catch (e: Throwable) {
                        ticketsErrorSingleLiveEvent.value = e.message?:""
                    }
                }
            }
        }
    }
}