package ua.blink.di

import android.app.Application
import android.content.SharedPreferences
import com.exhibition.ExhibitionApplication
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {
    fun sharedPreferences(): SharedPreferences
    fun application(): Application
    fun inject(exhibitionApplication: ExhibitionApplication)
}