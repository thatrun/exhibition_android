package ua.blink.di

import android.app.Application
import android.content.SharedPreferences
import androidx.room.Room
import com.exhibition.Constants
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val application: Application) {

    @Singleton
    @Provides
    fun providesApplication(): Application = application

    @Singleton
    @Provides
    fun providesShareds(): SharedPreferences = application.getSharedPreferences(Constants.SHARED_PREFERENCES, 0)
}