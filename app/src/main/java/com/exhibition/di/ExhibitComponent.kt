package com.exhibition.di

import com.exhibition.exhibition.ExhibitionDetailed
import dagger.Component
import ua.blink.di.main.MainComponent

@Exhibit
@Component(dependencies = [(MainComponent::class)], modules = [(ExhibitModule::class)])
interface ExhibitComponent {
    fun inject(exhibitionDetailed: ExhibitionDetailed)
}