package com.exhibition.di

import android.content.SharedPreferences
import com.exhibition.BlinkApi
import com.exhibition.Constants
import com.exhibition.exhibition.ExhibitsRecyclerAdapter
import com.exhibition.models.Exhibit
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class ExhibitModule {

    @Provides
    @com.exhibition.di.Exhibit
    fun providesAdapter(exhibits: ArrayList<Exhibit>): ExhibitsRecyclerAdapter = ExhibitsRecyclerAdapter(exhibits)

    @Provides
    @com.exhibition.di.Exhibit
    fun providesExhibitsList(): ArrayList<Exhibit> = arrayListOf()

    @Provides
    @com.exhibition.di.Exhibit
    fun providesBlinkApi(sharedPreferences: SharedPreferences): BlinkApi {
        val interceptor = object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val request: Request = chain.request().newBuilder()
                    .addHeader("Authorization",
                        "Bearer ${sharedPreferences.getString(Constants.TOKEN, "")}")
                    .build()
                return chain.proceed(request)
            }
        }

        val retrofit = Retrofit.Builder().baseUrl(Constants.BLINK_ENDPOINT)
            .client(OkHttpClient.Builder().addInterceptor(interceptor).build())
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder()
                        .setLenient()
                        .create()))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
        return retrofit.create(BlinkApi::class.java)
    }
}