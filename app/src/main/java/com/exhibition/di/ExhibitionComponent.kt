package com.exhibition.di

import com.exhibition.exhibition.ExhibitionFragment
import dagger.Component
import ua.blink.di.main.MainComponent

@Exhibition
@Component(dependencies = [(MainComponent::class)], modules = [(ExhibitionModule::class)])
interface ExhibitionComponent {
    fun inject(exhibitionFragment: ExhibitionFragment)
}