package ua.blink.di

import dagger.Component
import ua.blink.di.main.MainComponent
import ua.blink.ui.login.LoginFragment

@Login
@Component(dependencies = [(MainComponent::class)], modules = [(LoginModule::class)])
interface LoginComponent {
    fun inject(loginFragment: LoginFragment)
}