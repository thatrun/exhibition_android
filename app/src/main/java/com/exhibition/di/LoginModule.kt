package ua.blink.di

import android.content.SharedPreferences
import com.exhibition.BlinkLoginApi
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import ua.blink.ui.login.*

@Module
class LoginModule(val view: LoginView, val scope: CoroutineScope) {

    @Provides
    @Login
    fun provideView(): LoginView = view

    @Provides
    @Login
    fun providesScope(): CoroutineScope = scope

    @Provides
    @Login
    fun providePresenter(loginView: LoginView, loginModel: ua.blink.ui.login.LoginModel):
            LoginPresenter = LoginPresenterImpl(loginView, loginModel)

    @Provides
    @Login
    fun provideModule(blinkLoginApi: BlinkLoginApi, compositeJob: CoroutineScope,
                      sharedPreferences: SharedPreferences): LoginModel = LoginModelImpl(blinkLoginApi,
        sharedPreferences, compositeJob)
}