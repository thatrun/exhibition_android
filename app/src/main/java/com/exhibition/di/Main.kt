package ua.blink.di.main

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class Main