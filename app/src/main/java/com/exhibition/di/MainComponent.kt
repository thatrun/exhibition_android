package ua.blink.di.main

import android.app.Application
import android.content.SharedPreferences
import com.exhibition.BlinkLoginApi
import com.exhibition.MainActivity
import com.exhibition.MainViewModel
import dagger.Component
import ua.blink.di.AppComponent

@Main
@Component(dependencies = [(AppComponent::class)], modules = [(MainModule::class)])
interface MainComponent {
    fun sharedPreferences(): SharedPreferences
    fun inject(mainActivity: MainActivity)
    fun viewModel(): MainViewModel
    fun application(): Application
    fun loginApi(): BlinkLoginApi
}