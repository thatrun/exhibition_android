package ua.blink.di.main

import androidx.lifecycle.ViewModelProviders
import com.exhibition.BlinkLoginApi
import com.exhibition.Constants
import com.exhibition.MainActivity
import com.exhibition.MainViewModel
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class MainModule(val activity: MainActivity) {

    @Main
    @Provides
    fun providesViewModel(): MainViewModel = ViewModelProviders.of(activity).get(MainViewModel::class.java)

    @Main
    @Provides
    fun provideLoginApi(): BlinkLoginApi {
        val interceptor = object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val request: Request = chain.request().newBuilder()
                    .addHeader("Authorization",
                        "Basic ${Constants.BLINK_CLIENT_SECRET}")
                    .build()
                return chain.proceed(request)
            }
        }

        val retrofit = Retrofit.Builder().baseUrl(Constants.BLINK_ENDPOINT)
            .client(OkHttpClient.Builder().addInterceptor(interceptor).build())
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder()
                .setLenient()
                .create()))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
        return retrofit.create(BlinkLoginApi::class.java)
    }
}