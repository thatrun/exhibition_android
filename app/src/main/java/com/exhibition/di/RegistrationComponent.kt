package ua.blink.di

import dagger.Component
import ua.blink.di.main.MainComponent
import ua.blink.ui.registration.RegistrationFragment

@Registration
@Component(dependencies = [(MainComponent::class)], modules = [(RegistrationModule::class)])
interface RegistrationComponent {
    fun inject(registrationFragment: RegistrationFragment)
}