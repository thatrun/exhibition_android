package ua.blink.di

import android.content.SharedPreferences
import com.exhibition.BlinkLoginApi
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import ua.blink.ui.registration.*

@Module
class RegistrationModule(val registrationView: RegistrationView, val scope: CoroutineScope) {

    @Registration
    @Provides
    fun provideView(): RegistrationView = registrationView

    @Registration
    @Provides
    fun providePresenter(registrationView: RegistrationView,
                         registrationModel: RegistrationModel):
            RegistrationPresenter = RegistrationPresenterImpl(registrationView, registrationModel)

    @Registration
    @Provides
    fun providesScope(): CoroutineScope = scope

    @Registration
    @Provides
    fun provideRegistrationModel(blinkLoginApi: BlinkLoginApi,
                                 sharedPreferences: SharedPreferences,
                                 compositeJob: CoroutineScope):
            RegistrationModel = RegistrationModelImpl(blinkLoginApi, compositeJob, sharedPreferences)
}