package com.exhibition.di

import com.exhibition.ticket.TicketFragment
import dagger.Component
import ua.blink.di.main.MainComponent

@Ticket
@Component(dependencies = [(MainComponent::class)], modules = [(TicketModule::class)])
interface TicketComponent {
    fun inject(ticketFragment: TicketFragment)
}