package com.exhibition.exhibition

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.bumptech.glide.Glide
import com.exhibition.*
import com.exhibition.databinding.FragmentExhibitionDetailedBinding
import com.exhibition.di.DaggerExhibitComponent
import com.exhibition.di.ExhibitModule
import com.exhibition.models.Exhibit
import com.exhibition.models.Exhibition
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.ArrayList

class ExhibitionDetailed: Fragment() {

    lateinit var binder: FragmentExhibitionDetailedBinding
    @Inject
    lateinit var adapter: ExhibitsRecyclerAdapter
    @Inject
    lateinit var exhibits: ArrayList<Exhibit>
    @Inject
    lateinit var blinkApi: BlinkApi
    @Inject
    lateinit var mainViewModel: MainViewModel
    var ticketBroadcastReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.getIntExtra(Constants.WORKER_ERROR_TYPE, -1) == Constants.INTEREST_ERROR_TYPE &&
                intent.getStringExtra(Constants.ERROR_MESSAGE)?.isNotEmpty() == true && intent.getStringExtra(Constants.EVENT_ID).isNotEmpty()) {
                if (ExhibitionDetailedArgs.fromBundle(arguments!!).exhibition.isTicketPurchased == true) {
                    ExhibitionDetailedArgs.fromBundle(arguments!!).exhibition.isTicketPurchased = false
                    mainViewModel.tickets.remove(mainViewModel.tickets.find { it.exhibitionId == ExhibitionDetailedArgs.fromBundle(arguments!!).exhibition.exhibitionId })
                    mainViewModel.exhibitions.find { it.exhibitionId == ExhibitionDetailedArgs.fromBundle(arguments!!).exhibition.exhibitionId }?.isTicketPurchased = false
                    onBindExhibitionData()
                    if (intent.getStringExtra(Constants.ERROR_MESSAGE) == "Tickets sold out")
                        Toast.makeText(context, resources.getString(R.string.tickets_sold_out), Toast.LENGTH_LONG).show()
                }
            }
        }

    }
    var exhibitsPage: Int = 0
    var exhibitionId: String = ""
    lateinit var layoutManager: LinearLayoutManager
    var exhibitComponent = DaggerExhibitComponent.builder()
        .mainComponent(MainActivity.mainComponent)
        .exhibitModule(ExhibitModule())
        .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        exhibitComponent.inject(this)
    }

    override fun onResume() {
        super.onResume()
        (activity as? MainActivity?)?.hideBottomBar()
        (activity as? MainActivity?)?.hideActionBar()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_exhibition_detailed, container, false)
        setUpRecyclerView()
        return binder.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.registerReceiver(ticketBroadcastReceiver, IntentFilter(Constants.INTEREST_BROADCAST))
        onBindExhibitionData()
        onLoadExhibits()
        binder.detailedSwipeRefresh.setOnRefreshListener {
            exhibitsPage = 0
            exhibits = arrayListOf()
            onLoadExhibits()
        }
    }

    private fun onLoadExhibits() {
        if (exhibitsPage >= 0) {
            exhibitsPage += 1
            if (exhibitsPage == 1)
                binder.detailedSwipeRefresh.isRefreshing = true
            viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
                val result = blinkApi.getExhibits(exhibitsPage, Constants.PAGINATION_ITEMS_SIZE, exhibitionId)
                withContext(Dispatchers.Main) {
                    try {
                        val t = result.await()
                        val listType = object: TypeToken<ArrayList<Exhibit>>(){}.type
                        exhibits.addAll(Gson().fromJson(t.getAsJsonArray("content"), listType))
                        if (exhibitsPage >= t.getAsJsonPrimitive("totalPages").asInt)
                            exhibitsPage = -200
                        if (exhibits.size > 0) {
                            binder.detailedSimilarTitle.visibility = View.VISIBLE
                            binder.detailedSimilarRecyclerView.visibility = View.VISIBLE
                        }
                        binder.detailedSwipeRefresh.isRefreshing = false
                        adapter.notifyDataSetChanged()
                    } catch (e: Throwable) {
                        Toast.makeText(context, e.message?:"", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    private fun onBindExhibitionData() {
        val exhibition = ExhibitionDetailedArgs.fromBundle(arguments!!).exhibition
        exhibitionId = exhibition.exhibitionId?:""
        binder.detailedEventTitle.text = exhibition.title
        binder.detailedDateTime.text = Constants.dateFormat.format(Date(exhibition.startTime?:0L))
        binder.detailedDescription.text = exhibition.description
        if (exhibition.isTicketPurchased == true)
            showUserInterestedBtn()
        else
            showUserNotInterestedBtn(exhibition.price?.priceAmount?.toString()?:"", exhibition.price?.priceCurrency?:"")
        Glide.with(binder.detailedEventImage)
            .load(exhibition.image?:"")
            .into(binder.detailedEventImage)
        if (exhibition.isTicketPurchased == false) {
            binder.detailedInterestBtn.setOnClickListener {
                showUserInterestedBtn()
                exhibition.isTicketPurchased = true
                mainViewModel.tickets.add(exhibition)
                mainViewModel.exhibitions.find { it.exhibitionId == ExhibitionDetailedArgs.fromBundle(arguments!!).exhibition.exhibitionId }?.isTicketPurchased = true
                WorkManager.getInstance(context!!).beginWith(
                    OneTimeWorkRequest.Builder(InterestChangeWorker::class.java)
                        .addTag(Constants.INTERESTS_WORKER)
                        .setInputData(
                            Data.Builder()
                                .putString(Constants.EVENT_ID, ExhibitionDetailedArgs.fromBundle(arguments!!).exhibition.price?.priceId?:"")
                                .build())
                        .keepResultsForAtLeast(1, TimeUnit.SECONDS)
                        .build()).enqueue()
            }
        }
    }

    fun showUserInterestedBtn() {
        if (isAdded) {
            binder.detailedInterestBtn.background = ContextCompat.getDrawable(context!!, R.drawable.joined_btn_background)
            binder.detailedInterestBtn.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimaryDark))
            binder.detailedInterestBtn.text = resources.getString(R.string.purchased_ticket)
        }
    }

    fun showUserNotInterestedBtn(price: String, currency: String) {
        if (isAdded) {
            binder.detailedInterestBtn.background = ContextCompat.getDrawable(context!!, R.drawable.purple_btn_background)
            binder.detailedInterestBtn.setTextColor(ContextCompat.getColor(context!!, R.color.colorWhite))
            binder.detailedInterestBtn.text = "${resources.getString(R.string.buy_ticket)} ${resources.getString(R.string.for_money)} $price $currency"
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        context?.unregisterReceiver(ticketBroadcastReceiver)
    }

    private fun setUpRecyclerView() {
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binder.detailedSimilarRecyclerView.setHasFixedSize(true)
        binder.detailedSimilarRecyclerView.layoutManager = layoutManager
        binder.detailedSimilarRecyclerView.addItemDecoration(ProfileEventPreviewItemDecoration(resources.getDimensionPixelSize(R.dimen.safe_area_space)))
        binder.detailedSimilarRecyclerView.adapter = adapter
        binder.detailedSimilarRecyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition().plus(1)

                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= exhibits.size) {
                    onLoadExhibits()
                }
            }
        })
    }
}