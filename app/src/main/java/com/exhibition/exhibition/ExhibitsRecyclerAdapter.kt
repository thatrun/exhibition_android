package com.exhibition.exhibition

import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.exhibition.R
import com.exhibition.databinding.RecyclerExhibitItemBinding
import com.exhibition.models.Exhibit

class ExhibitsRecyclerAdapter(val exhibits: ArrayList<Exhibit>): RecyclerView.Adapter<ExhibitsRecyclerAdapter.ExhibitViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExhibitViewHolder = ExhibitViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context),
        R.layout.recycler_exhibit_item, parent, false))

    override fun getItemCount(): Int = exhibits.size

    override fun onBindViewHolder(holder: ExhibitViewHolder, position: Int) {
        val exhibit = exhibits[position]
        holder.binder.eventTitle.text = exhibit.title
        holder.binder.eventDescription.text = exhibit.description
        Glide.with(holder.binder.eventImage)
            .load(exhibit.image?:"")
            .into(holder.binder.eventImage)
    }


    inner class ExhibitViewHolder(val binder: RecyclerExhibitItemBinding): RecyclerView.ViewHolder(binder.root)
}

class ProfileEventPreviewItemDecoration(var margin: Int): RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.left = margin
        outRect.bottom = margin
        outRect.top = margin
        if (parent.adapter?.itemCount?.minus(1) == parent.getChildAdapterPosition(view))
            outRect.right = margin
    }
}