package ua.blink.ui.login

import android.app.Activity
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.ActivityNavigatorExtras
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.exhibition.Constants
import com.exhibition.MainActivity
import com.exhibition.R
import com.exhibition.databinding.FragmentLoginBinding
import ua.blink.di.DaggerLoginComponent
import ua.blink.di.LoginComponent
import ua.blink.di.LoginModule
import javax.inject.Inject

class LoginFragment: Fragment(), LoginView {

    lateinit var binder: FragmentLoginBinding
    lateinit var loginComponent: LoginComponent
    @Inject
    lateinit var loginPresenter: LoginPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        return binder.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginComponent = DaggerLoginComponent.builder()
            .mainComponent(MainActivity.mainComponent)
            .loginModule(LoginModule(this, viewLifecycleOwner.lifecycleScope))
            .build()
        loginComponent.inject(this)
        loginPresenter.onCheckIfUserAlreadySignedId()
        loginPresenter.onRestoreErrorsDisabledCount(arguments?.getInt(Constants.ERRORS_DISABLED_COUNT)?:0)
        loginPresenter.onRestoreErrorsCount(arguments?.getInt(Constants.ERRORS_COUNT)?:0)
        binder.loginSignUpNotice.setOnClickListener {
            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToRegistrationFragment())
        }
        binder.loginProgressBar.indeterminateDrawable.setColorFilter(ContextCompat.getColor(context!!, R.color.colorWhite),
            android.graphics.PorterDuff.Mode.MULTIPLY)
        binder.loginEnterBtn.alpha = 0.3f
        binder.loginEnterBtn.setOnClickListener {
            loginPresenter.onSignIn(binder.loginUsername.text.toString(),
                binder.loginPassword.text.toString())
        }
        binder.loginForgotPassword.setOnClickListener {
            loginPresenter.onOpenForgotFragment()
        }
        setUpSignUpSpan()
        setUpEditTextWatcher()
    }

    override fun openForgotFragment() {
        //findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToForgotFragment())
    }

    override fun onOpenResendVerificationFragment() {
        //findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToResendVerificationFragment())
    }

    private fun setUpSignUpSpan() {
        val spannableString = SpannableString(resources.getString(R.string.dont))
        val clickableSpan = object: ClickableSpan() {
            override fun onClick(p0: View) {

            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.color = ContextCompat.getColor(context!!, R.color.colorWhite)
                ds.isUnderlineText = false
            }

        }
        spannableString.setSpan(clickableSpan, spannableString.length.minus(resources.getString(R.string.signup).length),
            spannableString.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        binder.loginSignUpNotice.text = spannableString
        binder.loginSignUpNotice.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun setUpEditTextWatcher() {
        val textWatcher = object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                loginPresenter.onCheckIfFieldsFilled(binder.loginUsername.text.toString(),
                    binder.loginPassword.text.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        }
        binder.loginUsername.addTextChangedListener(textWatcher)
        binder.loginPassword.addTextChangedListener(textWatcher)
    }

    override fun changeEnterBtnAlpha(alpha: Float) {
        binder.loginEnterBtn.alpha = alpha
    }

    override fun hideLoginBtn() {
        binder.loginEnterBtn.visibility = View.INVISIBLE
    }

    override fun showLoginBtn() {
        binder.loginEnterBtn.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        binder.loginProgressBar.visibility = View.GONE
    }

    override fun showLoading() {
        binder.loginProgressBar.visibility = View.VISIBLE
    }

    override fun openManyErrorsFragment() {
        //findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToManyErrorsConfirmationFragment(), NavOptions.Builder().setPopUpTo(R.id.loginFragment, true).build())
    }

    override fun onDisplayNotReceivedEmailBtn() {
        //binder.loginNotReceivedEmail.visibility = View.VISIBLE
    }

    override fun onPause() {
        super.onPause()
        loginPresenter.onHideKeyboard()
        if (arguments == null)
            arguments = Bundle()
        arguments?.putInt(Constants.ERRORS_COUNT, loginPresenter.onGetErrorsCount())
        arguments?.putInt(Constants.ERRORS_DISABLED_COUNT, loginPresenter.onGetErrorsDisabledCount())
    }

    override fun userSignedIn() {
        findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToExhibitionFragment(), NavOptions.Builder().setPopUpTo(R.id.loginFragment, true).build())
    }

    override fun showMessage(message: Int) {
        Toast.makeText(context, resources.getString(message), Toast.LENGTH_SHORT).show()
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun showKeyboard() {
        if (activity != null && isAdded) {
            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(if (binder.loginUsername.text.isEmpty())
                binder.loginUsername else binder.loginPassword, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    override fun hideKeyboard() {
        if (context != null && isAdded) {
            val imm = context!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(if (binder.loginUsername.isFocused)
                binder.loginUsername.windowToken else
                binder.loginPassword.windowToken, 0)
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as? MainActivity?)?.hideActionBar()
        (activity as? MainActivity?)?.hideBottomBar()
        loginPresenter.onShowKeyboard()
    }

    override fun onDestroy() {
        super.onDestroy()
        loginPresenter.onDestroy()
    }
}