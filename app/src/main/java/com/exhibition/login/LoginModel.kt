package ua.blink.ui.login

interface LoginModel {

    interface LoginCallback {
        fun onUserLoggedIn()

        fun onError(message: String, description: String)
    }

    fun addCallback(loginCallback: LoginCallback)

    fun onCheckIfUserAlreadySignedId()

    fun onSignInUser(username: String, password: String)

    fun onDestroy()
}