package ua.blink.ui.login

import android.content.SharedPreferences
import com.exhibition.BlinkLoginApi
import com.exhibition.Constants
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class LoginModelImpl constructor(val blinkLoginApi: BlinkLoginApi, val sharedPreferences: SharedPreferences,
                                 val lifecycleScope: CoroutineScope): LoginModel {

    lateinit var loginCallback: LoginModel.LoginCallback

    override fun addCallback(loginCallback: LoginModel.LoginCallback) {
        this.loginCallback = loginCallback
    }

    override fun onSignInUser(username: String, password: String) {
        lifecycleScope.launch(Dispatchers.IO) {
            val result = blinkLoginApi.loginUser(username, password, "password")
            withContext(Dispatchers.Main) {
                try {
                    val t = result.await()
                    sharedPreferences.edit().putString(
                        Constants.TOKEN,
                        t.getAsJsonPrimitive("access_token").asString).putString(Constants.CURRENT_USERNAME, username)
                        .putString(Constants.REFRESH_TOKEN, t.getAsJsonPrimitive("refresh_token").asString).apply()
                    loginCallback.onUserLoggedIn()
                } catch (e: Throwable) {
                    val jsonObject = Gson().fromJson((e as? HttpException?)?.response()?.errorBody()?.string(), JsonObject::class.java)
                    val message = try {
                        jsonObject.getAsJsonPrimitive("error").asString
                    } catch (t: Throwable) {
                        e.message?:""
                    }
                    val description = try {
                        jsonObject.getAsJsonPrimitive("error_description").asString
                    } catch (t: Throwable) {
                        e.message?:""
                    }
                    loginCallback.onError(message, description)
                }
            }
        }
    }

    override fun onCheckIfUserAlreadySignedId() {
        if (sharedPreferences.getString(Constants.TOKEN, "").isNotEmpty()) {
            loginCallback.onUserLoggedIn()
        }
    }

    override fun onDestroy() {
    }
}