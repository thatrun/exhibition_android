package ua.blink.ui.login

interface LoginPresenter {

    fun onSignIn(username: String, password: String)

    fun onCheckIfFieldsFilled(username: String, password: String)

    fun onShowKeyboard()

    fun onHideKeyboard()

    fun onRestoreErrorsCount(errors: Int)

    fun onRestoreErrorsDisabledCount(errors: Int)

    fun onNotReceivedEmailClick()

    fun onCheckIfUserAlreadySignedId()

    fun onGetErrorsCount(): Int

    fun onGetErrorsDisabledCount(): Int

    fun onOpenForgotFragment()

    fun onDestroy()
}