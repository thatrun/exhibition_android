package ua.blink.ui.login

import com.exhibition.R
import javax.inject.Inject

class LoginPresenterImpl @Inject constructor(val loginView: LoginView, val loginModel: LoginModel):
    LoginPresenter, LoginModel.LoginCallback {

    init {
        loginModel.addCallback(this)
    }

    var errorCount: Int = 0
    var errorAccountDisabledCount: Int = 0

    override fun onSignIn(username: String, password: String) {
        when {
            username.isNotEmpty() && password.isNotEmpty() && password.length >= 6 -> {
                loginView.showLoading()
                loginView.hideLoginBtn()
                loginView.hideKeyboard()
                loginModel.onSignInUser(username, password)
            }
            username.isEmpty() -> {
                loginView.showMessage(R.string.username_length)
            }
            password.isEmpty() || password.length < 6 -> {
                loginView.showMessage(R.string.password_length)
            }
        }
    }

    override fun onCheckIfUserAlreadySignedId() {
        loginModel.onCheckIfUserAlreadySignedId()
    }

    override fun onCheckIfFieldsFilled(username: String, password: String) {
        if (username.isNotEmpty() && username.length >= 3 && password.isNotEmpty() && password.length >= 6)
            loginView.changeEnterBtnAlpha(1f)
        else
            loginView.changeEnterBtnAlpha(0.3f)
    }

    override fun onShowKeyboard() {
        loginView.showKeyboard()
    }

    override fun onHideKeyboard() {
        loginView.hideKeyboard()
    }

    override fun onUserLoggedIn() {
        loginView.hideLoading()
        loginView.showLoginBtn()
        loginView.userSignedIn()
    }

    override fun onOpenForgotFragment() {
        loginView.openForgotFragment()
    }

    override fun onError(message: String, description: String) {
        loginView.hideLoading()
        loginView.showLoginBtn()
        if (message == "invalid_grant" && description == "Bad credentials") {//creds problem
            loginView.showMessage(R.string.wrong_creds)
            errorCount += 1
            if (errorCount >= 8)
                loginView.openManyErrorsFragment()
        } else
            loginView.showMessage(message)
    }

    override fun onNotReceivedEmailClick() {
        loginView.onOpenResendVerificationFragment()
    }

    override fun onRestoreErrorsCount(errors: Int) {
        this.errorCount = errors
    }

    override fun onRestoreErrorsDisabledCount(errors: Int) {
        this.errorAccountDisabledCount = errors
        if (errorAccountDisabledCount >= 1)
            loginView.onDisplayNotReceivedEmailBtn()
    }

    override fun onGetErrorsDisabledCount(): Int = errorAccountDisabledCount

    override fun onGetErrorsCount(): Int = errorCount

    override fun onDestroy() {
        loginModel.onDestroy()
    }
}