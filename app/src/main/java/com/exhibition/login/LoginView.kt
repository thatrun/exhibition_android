package ua.blink.ui.login

interface LoginView {

    fun showLoading()

    fun hideLoading()

    fun showLoginBtn()

    fun hideLoginBtn()

    fun userSignedIn()

    fun openForgotFragment()

    fun openManyErrorsFragment()

    fun onDisplayNotReceivedEmailBtn()

    fun onOpenResendVerificationFragment()

    fun changeEnterBtnAlpha(alpha: Float)

    fun showMessage(message: String)

    fun showMessage(message: Int)

    fun showKeyboard()

    fun hideKeyboard()
}