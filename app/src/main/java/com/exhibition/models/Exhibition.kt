package com.exhibition.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*


class Exhibition() : Parcelable {
    @SerializedName("exhibitionId")
    @Expose
    var exhibitionId: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("description")
    @Expose
    var description: String? = null
    @SerializedName("image")
    @Expose
    var image: String? = null
    @SerializedName("startTime")
    @Expose
    var startTime: Long? = null
    @SerializedName("price")
    @Expose
    var price: Price? = null
    @SerializedName("ticketPurchased")
    @Expose
    var isTicketPurchased: Boolean? = null

    constructor(parcel: Parcel) : this() {
        exhibitionId = parcel.readString()
        title = parcel.readString()
        description = parcel.readString()
        image = parcel.readString()
        startTime = parcel.readValue(Long::class.java.classLoader) as? Long
        isTicketPurchased = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Exhibition) return false

        if (exhibitionId != other.exhibitionId) return false
        if (title != other.title) return false
        if (description != other.description) return false
        if (image != other.image) return false
        if (startTime != other.startTime) return false
        if (price != other.price) return false
        if (isTicketPurchased != other.isTicketPurchased) return false

        return true
    }

    override fun hashCode(): Int {
        var result = exhibitionId?.hashCode() ?: 0
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (image?.hashCode() ?: 0)
        result = 31 * result + (startTime?.hashCode() ?: 0)
        result = 31 * result + (price?.hashCode() ?: 0)
        result = 31 * result + (isTicketPurchased?.hashCode() ?: 0)
        return result
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(exhibitionId)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(image)
        parcel.writeValue(startTime)
        parcel.writeValue(isTicketPurchased)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Exhibition> {
        override fun createFromParcel(parcel: Parcel): Exhibition {
            return Exhibition(parcel)
        }

        override fun newArray(size: Int): Array<Exhibition?> {
            return arrayOfNulls(size)
        }
    }


}

class Exhibit {
    @SerializedName("exhibitId")
    @Expose
    var exhibitId: String? = UUID.randomUUID().toString()
    @SerializedName("title")
    @Expose
    var title: String? = ""
    @SerializedName("image")
    @Expose
    var image: String? = ""
    @SerializedName("description")
    @Expose
    var description: String? = ""

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Exhibit) return false

        if (exhibitId != other.exhibitId) return false
        if (title != other.title) return false
        if (image != other.image) return false
        if (description != other.description) return false

        return true
    }

    override fun hashCode(): Int {
        var result = exhibitId?.hashCode() ?: 0
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (image?.hashCode() ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        return result
    }


}

class Price() : Parcelable {
    @SerializedName("priceId")
    @Expose
    var priceId: String? = null
    @SerializedName("priceAmount")
    @Expose
    var priceAmount: Int? = null
    @SerializedName("ticketsMaximumCount")
    @Expose
    var ticketsMaximumCount: Int? = null
    @SerializedName("priceCurrency")
    @Expose
    var priceCurrency: String? = null

    constructor(parcel: Parcel) : this() {
        priceId = parcel.readString()
        priceAmount = parcel.readValue(Int::class.java.classLoader) as? Int
        priceCurrency = parcel.readString()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Price) return false

        if (priceId != other.priceId) return false
        if (priceAmount != other.priceAmount) return false
        if (priceCurrency != other.priceCurrency) return false

        return true
    }

    override fun hashCode(): Int {
        var result = priceId?.hashCode() ?: 0
        result = 31 * result + (priceAmount ?: 0)
        result = 31 * result + (priceCurrency?.hashCode() ?: 0)
        return result
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(priceId)
        parcel.writeValue(priceAmount)
        parcel.writeString(priceCurrency)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Price> {
        override fun createFromParcel(parcel: Parcel): Price {
            return Price(parcel)
        }

        override fun newArray(size: Int): Array<Price?> {
            return arrayOfNulls(size)
        }
    }


}