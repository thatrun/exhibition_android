package ua.blink.ui.registration

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.exhibition.MainActivity
import com.exhibition.R
import com.exhibition.databinding.FragmentRegistrationBinding
import ua.blink.di.DaggerRegistrationComponent
import ua.blink.di.RegistrationComponent
import ua.blink.di.RegistrationModule
import javax.inject.Inject

class RegistrationFragment: Fragment(), RegistrationView {

    lateinit var binder: FragmentRegistrationBinding
    lateinit var registrationComponent: RegistrationComponent
    @Inject
    lateinit var registrationPresenter: RegistrationPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_registration, container, false)
        return binder.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registrationComponent = DaggerRegistrationComponent.builder()
            .mainComponent(MainActivity.mainComponent)
            .registrationModule(RegistrationModule(this, viewLifecycleOwner.lifecycleScope))
            .build()
        registrationComponent.inject(this)
        binder.registrationEnterBtn.setOnClickListener {
            registrationPresenter.onRegisterClick(binder.registrationPassword.text.toString(),
                binder.registrationEmail.text.toString())
        }
        binder.registrationEnterBtn.alpha = 0.3f
        binder.registerProgressBar.indeterminateDrawable.setColorFilter(
            ContextCompat.getColor(context!!, R.color.colorWhite),
            android.graphics.PorterDuff.Mode.MULTIPLY)
        setUpEditTextWatcher()
    }

    override fun onResume() {
        super.onResume()
        (activity as? MainActivity?)?.hideActionBar()
        (activity as? MainActivity?)?.hideBottomBar()
    }

    override fun hideKeyboard() {
        if (context != null && isAdded) {
            val imm = context!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(when {
                binder.registrationEmail.isFocused -> binder.registrationEmail.windowToken
                binder.registrationPassword.isFocused -> binder.registrationPassword.windowToken
                else -> binder.registrationEmail.windowToken
            }, 0)
        }
    }

    private fun setUpEditTextWatcher() {
        val textWatcher = object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                registrationPresenter.onCheckIfFieldsFilled(binder.registrationEmail.text.toString(), binder.registrationPassword.text.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        }
        binder.registrationPassword.addTextChangedListener(textWatcher)
        binder.registrationEmail.addTextChangedListener(textWatcher)
    }

    override fun onPause() {
        super.onPause()
        registrationPresenter.onHideKeyboard()
    }

    override fun changeEnterBtnAlpha(alpha: Float) {
        binder.registrationEnterBtn.alpha = alpha
    }

    override fun showMessage(message: Int) {
        Toast.makeText(context, resources.getString(message), Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        registrationPresenter.onDestroy()
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun hideLoading() {
        binder.registerProgressBar.visibility = View.GONE
    }

    override fun showLoading() {
        binder.registerProgressBar.visibility = View.VISIBLE
    }

    override fun hideRegisterBtn() {
        binder.registrationEnterBtn.visibility = View.INVISIBLE
    }

    override fun showRegisterBtn() {
        binder.registrationEnterBtn.visibility = View.VISIBLE
    }

    override fun userRegistred() {
        activity?.onBackPressed()
    }
}