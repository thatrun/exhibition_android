package ua.blink.ui.registration

interface RegistrationModel {

    interface RegistrationCallback {
        fun onUserRegistred()

        fun onError(error: String)

        fun onError(error: Int)
    }

    fun registerUser(password: String, email: String)

    fun addCallback(registrationCallback: RegistrationCallback)

    fun onDestroy()
}