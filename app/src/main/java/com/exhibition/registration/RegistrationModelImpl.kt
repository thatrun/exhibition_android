package ua.blink.ui.registration

import android.content.SharedPreferences
import com.exhibition.BlinkLoginApi
import com.exhibition.R
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import javax.inject.Inject

class RegistrationModelImpl @Inject constructor(val blinkLoginApi: BlinkLoginApi, val lifecycleScope: CoroutineScope,
                                                val sharedPreferences: SharedPreferences): RegistrationModel {

    lateinit var registrationCallback: RegistrationModel.RegistrationCallback

    override fun addCallback(registrationCallback: RegistrationModel.RegistrationCallback) {
        this.registrationCallback = registrationCallback
    }

    override fun registerUser(password: String, email: String) {
        lifecycleScope.launch(Dispatchers.IO) {
            val result = blinkLoginApi.registerUser(email, password)
            withContext(Dispatchers.Main) {
                try {
                    result.await()
                    registrationCallback.onUserRegistred()
                } catch (e: Throwable) {
                    val message = try {
                        Gson().fromJson((e as? HttpException?)?.response()?.errorBody()?.
                            string(), JsonObject::class.java).get("error").asJsonPrimitive.asString
                    } catch (t: Throwable) {
                        ""
                    }
                    val code = (e as? HttpException?)?.response()?.code()
                    when {
                        code == 400 && message == "Something went wrong" ->
                            registrationCallback.onError(R.string.something_went_wrong)
                        code == 400 && message == "Username already taken" ->
                            registrationCallback.onError(R.string.username_registred_already)
                        code == 400 && message == "User with this email already exist" ->
                            registrationCallback.onError(R.string.email_registered)
                        code == 400 && message == "Username is not valid" ->
                            registrationCallback.onError(R.string.username_not_valid)
                        else -> registrationCallback.onError("${(e as? HttpException?)?.response()?.errorBody()?.
                            string()} ${e.message}")
                    }
                }
            }
        }
    }

    override fun onDestroy() {
    }
}
