package ua.blink.ui.registration

interface RegistrationPresenter {

    fun onRegisterClick(password: String,
                        email: String)

    fun onCheckIfFieldsFilled(email: String, password: String)

    fun onHideKeyboard()

    fun onDestroy()
}