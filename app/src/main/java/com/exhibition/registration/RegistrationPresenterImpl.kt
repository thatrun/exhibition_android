package ua.blink.ui.registration

import android.util.Patterns
import com.exhibition.R
import javax.inject.Inject

class RegistrationPresenterImpl @Inject constructor(val registrationView: RegistrationView,
                                                        val registrationModel: RegistrationModel):
    RegistrationPresenter, RegistrationModel.RegistrationCallback {

    init {
        registrationModel.addCallback(this)
    }

    override fun onRegisterClick(password: String, email: String) {
        when {
            password.isNotEmpty() &&
                    password.length >= 6 && email.isNotEmpty() &&
                    Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                registrationView.showLoading()
                registrationView.hideRegisterBtn()
                registrationView.hideKeyboard()
                registrationModel.registerUser(password, email)
            }
            email.isEmpty() -> {
                registrationView.showMessage(R.string.email_empty)
            }
            !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                registrationView.showMessage(R.string.email_not_valid)
            }
            password.isEmpty() -> {
                registrationView.showMessage(R.string.password_empty)
            }
            password.length < 6 -> {
                registrationView.showMessage(R.string.password_length)
            }
        }
    }

    override fun onHideKeyboard() {
        registrationView.hideKeyboard()
    }

    override fun onCheckIfFieldsFilled(email: String, password: String) {
        if (password.isNotEmpty() &&
            password.length >= 6 && email.isNotEmpty() &&
            Patterns.EMAIL_ADDRESS.matcher(email).matches())
            registrationView.changeEnterBtnAlpha(1f)
        else
            registrationView.changeEnterBtnAlpha(0.3f)
    }

    override fun onError(error: Int) {
        registrationView.showMessage(error)
        registrationView.hideLoading()
        registrationView.showRegisterBtn()
    }

    override fun onUserRegistred() {
        registrationView.hideLoading()
        registrationView.showRegisterBtn()
        registrationView.userRegistred()
    }

    override fun onError(error: String) {
        registrationView.hideLoading()
        registrationView.showRegisterBtn()
        registrationView.showMessage(error)
    }

    override fun onDestroy() {
        registrationModel.onDestroy()
    }
}