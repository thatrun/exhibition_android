package ua.blink.ui.registration

interface RegistrationView {

    fun showMessage(message: Int)

    fun userRegistred()

    fun showLoading()

    fun hideLoading()

    fun showRegisterBtn()

    fun hideRegisterBtn()

    fun changeEnterBtnAlpha(alpha: Float)

    fun showMessage(message: String)

    fun hideKeyboard()
}