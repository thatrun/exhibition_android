package com.exhibition.ticket

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.exhibition.*
import com.exhibition.databinding.FragmentExhibitionBinding
import com.exhibition.di.DaggerExhibitionComponent
import com.exhibition.di.DaggerTicketComponent
import com.exhibition.di.ExhibitionModule
import com.exhibition.di.TicketModule
import com.exhibition.exhibition.PopularRecyclerAdapter
import javax.inject.Inject

class TicketFragment: Fragment(), ExhibitionClickListener {
    lateinit var layoutManager: LinearLayoutManager
    lateinit var smoothScroller: LinearSmoothScroller
    lateinit var binder: FragmentExhibitionBinding
    @Inject
    lateinit var popularAdapter: TicketsRecyclerAdapter
    @Inject
    lateinit var mainViewModel: MainViewModel
    @Inject
    lateinit var blinkApi: BlinkApi
    var ticketComponent = DaggerTicketComponent.builder()
        .mainComponent(MainActivity.mainComponent)
        .ticketModule(TicketModule(this))
        .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ticketComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_exhibition, container, false)
        binder.popularProgressBar.indeterminateDrawable.setColorFilter(
            ContextCompat.getColor(context!!, R.color.buttonColor),
            PorterDuff.Mode.SRC_IN)
        return binder.root
    }

    override fun onResume() {
        super.onResume()
        (activity as? MainActivity?)?.showBottomBar()
        (activity as? MainActivity?)?.showActionBar()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        mainViewModel.ticketsErrorSingleLiveEvent.singleObserve(viewLifecycleOwner, Observer {
            binder.popularProgressBar.visibility = View.GONE
            binder.popularSwipeRefresh.isRefreshing = false
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        })
        mainViewModel.ticketsLiveData.observe(viewLifecycleOwner, Observer {
            if (it.first > 0 || it.first < 0)
                binder.popularProgressBar.visibility = View.GONE
            binder.popularSwipeRefresh.isRefreshing = false
            binder.popularRecyclerView.visibility = View.VISIBLE
            popularAdapter.notifyDataSetChanged()
        })
        binder.popularSwipeRefresh.setOnRefreshListener {
            mainViewModel.ticketsPage = 0
            mainViewModel.tickets = arrayListOf()
            mainViewModel.onLoadTickets(blinkApi)
        }
        mainViewModel.onLoadTickets(blinkApi)
    }

    override fun onClick(position: Int) {
        findNavController().navigate(TicketFragmentDirections.actionTicketFragmentToExhibitionDetailed(mainViewModel.tickets[position]))
    }

    private fun setUpRecyclerView() {
        layoutManager = LinearLayoutManager(context)
        smoothScroller = object: LinearSmoothScroller(context) {
            override fun getVerticalSnapPreference(): Int {
                return LinearSmoothScroller.SNAP_TO_START
            }
        }
        binder.popularRecyclerView.setHasFixedSize(true)
        binder.popularRecyclerView.layoutManager = layoutManager
        binder.popularRecyclerView.adapter = popularAdapter
        binder.popularRecyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition().plus(1)

                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= mainViewModel.tickets.size) {
                    mainViewModel.onLoadTickets(blinkApi)
                }
            }
        })
    }
}