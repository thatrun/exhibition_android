package com.exhibition.ticket

import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.exhibition.Constants
import com.exhibition.ExhibitionClickListener
import com.exhibition.MainViewModel
import com.exhibition.R
import com.exhibition.databinding.RecyclerPopularItemBinding
import java.util.*
import javax.inject.Inject


class TicketsRecyclerAdapter @Inject constructor(val mainViewModel: MainViewModel, val exhibitionClickListener: ExhibitionClickListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = PopularViewHolder(
        DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.recycler_popular_item, parent, false))

    override fun getItemCount(): Int = mainViewModel.tickets.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder as? PopularViewHolder? != null) {
            val ticket = mainViewModel.tickets[position]
            holder.binder.eventDateMonth.text = Constants.monthDateFormat.format(Date(ticket.startTime?:0L))
            holder.binder.eventDateNumber.text = Constants.dayDateFormat.format(Date(ticket.startTime?:0L))
            holder.binder.eventDescription.text = ticket.description?:""
            holder.binder.eventTitle.text = ticket.title?:""
            Glide.with(holder.binder.eventImage)
                .load(ticket.image?:"")
                .into(holder.binder.eventImage)
            holder.binder.root.setOnClickListener {
                exhibitionClickListener.onClick(holder.adapterPosition)
            }
        }
    }

    inner class PopularViewHolder(val binder: RecyclerPopularItemBinding): RecyclerView.ViewHolder(binder.root)

}

class PopularItemDecoration(val bottomMargin: Int): RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.getChildAdapterPosition(view) != parent.adapter?.itemCount?.minus(1)) {
            outRect.bottom = bottomMargin
        }
    }
}